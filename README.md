## Install npm package

```
npm install
```

## Install Sequelize CLI

```
npm i sequelize-cli -g
```

## Create DB

```
sequelize db:create
```

## Migrate DB

```
sequelize db:migrate
```

## Create new table

```
sequelize model:generate --name TABLE_NAME --attributes PARAM1:VALUE1,PARAM2:VALUE2
```